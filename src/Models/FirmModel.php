<?php


namespace GoCRM\API\App\Models;


class FirmModel extends Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nameFirm;

    /**
     * @var string|null
     */
    private $legalAddress;

    /**
     * @var string|null
     */
    private $actualAddress;

    /**
     * @var string|null
     */
    private $phone;

    /**
     * @var string|null
     */
    private $fax;

    /**
     * @var string|null
     */
    private $email;

    /**
     * @var string|null
     */
    private $ogrn;

    /**
     * @var string|null
     */
    private $inn;

    /**
     * @var string|null
     */
    private $kpp;

    /**
     * @var string|null
     */
    private $director;

    /**
     * @var string|null
     */
    private $base;

    /**
     * @var string|null
     */
    private $bank;

    /**
     * @var string|null
     */
    private $bik;

    /**
     * @var string|null
     */
    private $rs;

    /**
     * @var string|null
     */
    private $ks;

    /**
     * @var string|null
     */
    private $doc;

    /**
     * @var string|null
     */
    private $welcomeMessage;

    /**
     * @var \DateTimeInterface
     */
    private $createdAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNameFirm(): string
    {
        return $this->nameFirm;
    }

    /**
     * @param string $nameFirm
     */
    public function setNameFirm(string $nameFirm): void
    {
        $this->nameFirm = $nameFirm;
    }

    /**
     * @return string|null
     */
    public function getLegalAddress(): ?string
    {
        return $this->legalAddress;
    }

    /**
     * @param string|null $legalAddress
     */
    public function setLegalAddress(?string $legalAddress): void
    {
        $this->legalAddress = $legalAddress;
    }

    /**
     * @return string|null
     */
    public function getActualAddress(): ?string
    {
        return $this->actualAddress;
    }

    /**
     * @param string|null $actualAddress
     */
    public function setActualAddress(?string $actualAddress): void
    {
        $this->actualAddress = $actualAddress;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string|null
     */
    public function getFax(): ?string
    {
        return $this->fax;
    }

    /**
     * @param string|null $fax
     */
    public function setFax(?string $fax): void
    {
        $this->fax = $fax;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getOgrn(): ?string
    {
        return $this->ogrn;
    }

    /**
     * @param string|null $ogrn
     */
    public function setOgrn(?string $ogrn): void
    {
        $this->ogrn = $ogrn;
    }

    /**
     * @return string|null
     */
    public function getInn(): ?string
    {
        return $this->inn;
    }

    /**
     * @param string|null $inn
     */
    public function setInn(?string $inn): void
    {
        $this->inn = $inn;
    }

    /**
     * @return string|null
     */
    public function getKpp(): ?string
    {
        return $this->kpp;
    }

    /**
     * @param string|null $kpp
     */
    public function setKpp(?string $kpp): void
    {
        $this->kpp = $kpp;
    }

    /**
     * @return string|null
     */
    public function getDirector(): ?string
    {
        return $this->director;
    }

    /**
     * @param string|null $director
     */
    public function setDirector(?string $director): void
    {
        $this->director = $director;
    }

    /**
     * @return string|null
     */
    public function getBase(): ?string
    {
        return $this->base;
    }

    /**
     * @param string|null $base
     */
    public function setBase(?string $base): void
    {
        $this->base = $base;
    }

    /**
     * @return string|null
     */
    public function getBank(): ?string
    {
        return $this->bank;
    }

    /**
     * @param string|null $bank
     */
    public function setBank(?string $bank): void
    {
        $this->bank = $bank;
    }

    /**
     * @return string|null
     */
    public function getBik(): ?string
    {
        return $this->bik;
    }

    /**
     * @param string|null $bik
     */
    public function setBik(?string $bik): void
    {
        $this->bik = $bik;
    }

    /**
     * @return string|null
     */
    public function getRs(): ?string
    {
        return $this->rs;
    }

    /**
     * @param string|null $rs
     */
    public function setRs(?string $rs): void
    {
        $this->rs = $rs;
    }

    /**
     * @return string|null
     */
    public function getKs(): ?string
    {
        return $this->ks;
    }

    /**
     * @param string|null $ks
     */
    public function setKs(?string $ks): void
    {
        $this->ks = $ks;
    }

    /**
     * @return string|null
     */
    public function getDoc(): ?string
    {
        return $this->doc;
    }

    /**
     * @param string|null $doc
     */
    public function setDoc(?string $doc): void
    {
        $this->doc = $doc;
    }

    /**
     * @return string|null
     */
    public function getWelcomeMessage(): ?string
    {
        return $this->welcomeMessage;
    }

    /**
     * @param string|null $welcomeMessage
     */
    public function setWelcomeMessage(?string $welcomeMessage): void
    {
        $this->welcomeMessage = $welcomeMessage;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeInterface $createdAt
     */
    public function setCreatedAt(?\DateTimeInterface $createdAt = null): void
    {
        $this->createdAt = $createdAt;
    }

    public function fill(array $attributes = []): ?ModelInterface
    {
        $this->setId($attributes['id']);
        $this->setNameFirm($attributes['nameFirm']);
        $this->setLegalAddress($attributes['legalAddress']??null);
        $this->setActualAddress($attributes['actualAddress']??null);
        $this->setPhone($attributes['phone']??null);
        $this->setFax($attributes['fax']??null);
        $this->setEmail($attributes['email']??null);
        $this->setOgrn($attributes['ogrn']??null);
        $this->setInn($attributes['inn']??null);
        $this->setKpp($attributes['kpp']??null);
        $this->setDirector($attributes['director']??null);
        $this->setBase($attributes['base']??null);
        $this->setBank($attributes['bank']??null);
        $this->setBik($attributes['bik']??null);
        $this->setRs($attributes['rs']??null);
        $this->setKs($attributes['ks']??null);
        $this->setDoc($attributes['doc']??null);
        $this->setWelcomeMessage($attributes['welcomeMessage']??null);
        $this->setCreatedAt(!empty($attributes['createdAt']) ? \DateTime::createFromFormat($this->dateTimeFormat, $attributes['createdAt']) : null);

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'nameFirm' => $this->nameFirm,
            'legalAddress' => $this->legalAddress,
            'actualAddress' => $this->actualAddress,
            'phone' => $this->phone,
            'fax' => $this->fax,
            'email' => $this->email,
            'ogrn' => $this->ogrn,
            'inn' => $this->inn,
            'kpp' => $this->kpp,
            'director' => $this->director,
            'base' => $this->base,
            'bank' => $this->bank,
            'bik' => $this->bik,
            'rs' => $this->rs,
            'ks' => $this->ks,
            'doc' => $this->doc,
            'welcomeMessage' => $this->welcomeMessage,
            'createdAt' => !empty($this->createdAt) ? $this->createdAt->format($this->dateTimeFormat) : null,
        ];
    }

}