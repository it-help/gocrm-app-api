<?php


namespace GoCRM\API\App\Models;


class UserModel extends Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string|null
     */
    private $displayName;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var string|null
     */
    private $photo;

    /**
     * @var int
     */
    private $position;

    /**
     * @var int
     */
    private $centerId;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getDisplayName(): ?string
    {
        return $this->displayName;
    }

    /**
     * @param string|null $displayName
     */
    public function setDisplayName(?string $displayName): void
    {
        $this->displayName = $displayName;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    /**
     * @param string|null $photo
     */
    public function setPhoto(?string $photo): void
    {
        $this->photo = $photo;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    /**
     * @return int
     */
    public function getCenterId(): int
    {
        return $this->centerId;
    }

    /**
     * @param int $centerId
     */
    public function setCenterId(int $centerId): void
    {
        $this->centerId = $centerId;
    }

    public function fill(array $attributes = []): ?ModelInterface
    {
        $this->setId($attributes['id']);
        $this->setLogin($attributes['login']);
        $this->setName($attributes['name']);
        $this->setDisplayName($attributes['displayName']??null);
        $this->setDescription($attributes['description']??null);
        $this->setPhoto($attributes['photo']??null);
        $this->setPosition($attributes['position']);
        $this->setCenterId($attributes['centerId']);

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'login' => $this->login,
            'name' => $this->name,
            'displayName' => $this->displayName,
            'description' => $this->description,
            'photo' => $this->photo,
            'position' => $this->position,
            'centerId' => $this->centerId
        ];
    }

}
