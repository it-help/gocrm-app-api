<?php


namespace GoCRM\API\App\Models;


abstract class Model implements ModelInterface
{
    protected $dateTimeFormat = 'Y-m-d H:i:s';

    /**
     * @return string
     */
    public function toJson(): string
    {
        return json_encode($this->toArray());
    }

    /**
     * @param array $attributes
     * @return ModelInterface
     */
    public static function newInstance(array $attributes = []): ModelInterface
    {
        $instance = new static();
        $instance->fill($attributes);

        return $instance;
    }


}