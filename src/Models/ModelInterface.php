<?php


namespace GoCRM\API\App\Models;


interface ModelInterface
{
    /**
     * @param array $attributes
     * @return ModelInterface|null
     */
    public function fill(array $attributes = []): ?ModelInterface;

    /**
     * @return array
     */
    public function toArray(): array;

    /**
     * @return string
     */
    public function toJson(): string;

    public static function newInstance(array $attributes = []): ModelInterface;
}