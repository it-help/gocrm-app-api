<?php


namespace GoCRM\API\App\Models;


class CenterModel extends Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $cityAlias;

    /**
     * @var string
     */
    private $centerAlias;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string|null
     */
    private $phone;

    /**
     * @var string|null
     */
    private $email;

    /**
     * @var int
     */
    private $cityId;

    /**
     * @var int
     */
    private $firmId;

    /**
     * @var \DateTimeZone
     */
    private $timezone;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var float|null
     */
    private $latitude;

    /**
     * @var float|null
     */
    private $longitude;

    /**
     * @var int|null
     */
    private $siteStatus;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getCityAlias(): string
    {
        return $this->cityAlias;
    }

    /**
     * @param string $cityAlias
     */
    public function setCityAlias(string $cityAlias): void
    {
        $this->cityAlias = $cityAlias;
    }

    /**
     * @return string
     */
    public function getCenterAlias(): string
    {
        return $this->centerAlias;
    }

    /**
     * @param string $centerAlias
     */
    public function setCenterAlias(string $centerAlias): void
    {
        $this->centerAlias = $centerAlias;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getCityId(): int
    {
        return $this->cityId;
    }

    /**
     * @param int $cityId
     */
    public function setCityId(int $cityId): void
    {
        $this->cityId = $cityId;
    }

    /**
     * @return int
     */
    public function getFirmId(): int
    {
        return $this->firmId;
    }

    /**
     * @param int $firmId
     */
    public function setFirmId(int $firmId): void
    {
        $this->firmId = $firmId;
    }

    /**
     * @return \DateTimeZone
     */
    public function getTimezone(): \DateTimeZone
    {
        return $this->timezone;
    }

    /**
     * @param \DateTimeZone $timezone
     */
    public function setTimezone(\DateTimeZone $timezone): void
    {
        $this->timezone = $timezone;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description = null): void
    {
        $this->description = $description;
    }

    /**
     * @return float|null
     */
    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    /**
     * @param float|null $latitude
     */
    public function setLatitude(?float $latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @return float|null
     */
    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    /**
     * @param float|null $longitude
     */
    public function setLongitude(?float $longitude): void
    {
        $this->longitude = $longitude;
    }

    /**
     * @return int|null
     */
    public function getSiteStatus(): ?int
    {
        return $this->siteStatus;
    }

    /**
     * @param int|null $siteStatus
     */
    public function setSiteStatus(?int $siteStatus): void
    {
        $this->siteStatus = $siteStatus;
    }

    public function fill(array $attributes = []): ?ModelInterface
    {
        $this->setId($attributes['id']);
        $this->setCity($attributes['city']);
        $this->setCityAlias($attributes['cityAlias']);
        $this->setCenterAlias($attributes['centerAlias']);
        $this->setAddress($attributes['address']);
        $this->setPhone($attributes['phone']??null);
        $this->setEmail($attributes['email']??null);
        $this->setCityId($attributes['cityId']);
        $this->setFirmId($attributes['firmId']);
        $this->setTimezone(new \DateTimeZone($attributes['timezone']));
        $this->setDescription($attributes['description']??null);
        $this->setLatitude($attributes['latitude']??null);
        $this->setLongitude($attributes['longitude']??null);
        $this->setSiteStatus($attributes['siteStatus'] ?? 0);

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'city' => $this->city,
            'cityAlias' => $this->cityAlias,
            'centerAlias' => $this->centerAlias,
            'address' => $this->address,
            'phone' => $this->phone,
            'email' => $this->email,
            'cityId' => $this->cityId,
            'firmId' => $this->firmId,
            'timezone' => $this->timezone->getName(),
            'description' => $this->description,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'siteStatus' => $this->siteStatus
        ];
    }

}