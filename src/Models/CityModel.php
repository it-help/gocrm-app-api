<?php


namespace GoCRM\API\App\Models;


class CityModel extends Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $alias;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string|null
     */
    private $site;

    /**
     * @var int
     */
    private $countryId;

    /**
     * @var int|null
     */
    private $mobileAppWayId;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     */
    public function setAlias(string $alias): void
    {
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getSite(): ?string
    {
        return $this->site;
    }

    /**
     * @param string|null $site
     */
    public function setSite(?string $site): void
    {
        $this->site = $site;
    }

    /**
     * @return int
     */
    public function getCountryId(): int
    {
        return $this->countryId;
    }

    /**
     * @param int $countryId
     */
    public function setCountryId(int $countryId): void
    {
        $this->countryId = $countryId;
    }

    /**
     * @return int|null
     */
    public function getMobileAppWayId(): ?int
    {
        return $this->mobileAppWayId;
    }

    /**
     * @param int|null $mobileAppWayId
     */
    public function setMobileAppWayId(?int $mobileAppWayId): void
    {
        $this->mobileAppWayId = $mobileAppWayId;
    }

    public function fill(array $attributes = []): ?ModelInterface
    {
        $this->setId($attributes['id']);
        $this->setAlias($attributes['alias']);
        $this->setName($attributes['name']);
        $this->setCountryId($attributes['countryId']);
        $this->setSite($attributes['site']??null);
        $this->setMobileAppWayId($attributes['mobileAppWayId']??null);

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'alias' => $this->alias,
            'name' => $this->name,
            'countryId' => $this->countryId,
            'site' => $this->site,
            'mobileAppWayId' => $this->mobileAppWayId,
        ];
    }

}
