<?php


namespace GoCRM\API\App\Repositories;


class UserRepository extends ApplicationRepositoryAbstract
{
    protected $path = '/users';


    public function all(): array
    {
        $response = $this->request->get('/');

        if ($response->getStatus() === 'success') {
            if (is_array($response->data())) {
                return array_map([\GoCRM\API\App\Models\UserModel::class, 'newInstance'], $response->data());
            }
        }

        return [];
    }

    public function find(int $id): ? \GoCRM\API\App\Models\UserModel
    {
        $response = $this->request->get('/'.$id);

        if ($response->getStatus() === 'success') {
            return \GoCRM\API\App\Models\UserModel::newInstance($response->data());
        }

        return null;
    }
}