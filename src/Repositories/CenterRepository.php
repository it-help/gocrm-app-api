<?php


namespace GoCRM\API\App\Repositories;


class CenterRepository extends ApplicationRepositoryAbstract
{
    protected $path = '/centers';

    public function all(): array
    {
        $response = $this->request->get('/');

        if ($response->getStatus() === 'success') {
            return array_map([\GoCRM\API\App\Models\CenterModel::class, 'newInstance'], $response->data());
        }

        return [];
    }

    public function find(int $id): ? \GoCRM\API\App\Models\CenterModel
    {
        $response = $this->request->get('/'.$id);

        if ($response->getStatus() === 'success') {
            return \GoCRM\API\App\Models\CenterModel::newInstance($response->data());
        }

        return null;
    }
}