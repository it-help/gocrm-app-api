<?php


namespace GoCRM\API\App\Repositories;


class CityRepository extends ApplicationRepositoryAbstract
{
    protected $path = '/cities';

    public function all(): array
    {
        $response = $this->request->get('/');

        if ($response->getStatus() === 'success') {
            return array_map([\GoCRM\API\App\Models\CityModel::class, 'newInstance'], $response->data());
        }

        return [];
    }

    public function find(int $id): ? \GoCRM\API\App\Models\CityModel
    {
        $response = $this->request->get('/'.$id);

        if ($response->getStatus() === 'success') {
            return \GoCRM\API\App\Models\CityModel::newInstance($response->data());
        }

        return null;
    }
}