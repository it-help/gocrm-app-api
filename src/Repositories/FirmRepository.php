<?php


namespace GoCRM\API\App\Repositories;


class FirmRepository extends ApplicationRepositoryAbstract
{
    protected $path = '/firms';


    public function all(): array
    {
        $response = $this->request->get('/');

        if ($response->getStatus() === 'success') {
            return array_map([\GoCRM\API\App\Models\FirmModel::class, 'newInstance'], $response->data());
        }

        return [];
    }

    public function find(int $id): ? \GoCRM\API\App\Models\FirmModel
    {
        $response = $this->request->get('/'.$id);

        if ($response->getStatus() === 'success') {
            return \GoCRM\API\App\Models\FirmModel::newInstance($response->data());
        }

        return null;
    }
}
