<?php


namespace GoCRM\API\App;


use GoCRM\API\App\Repositories\CenterRepository;
use GoCRM\API\App\Repositories\CityRepository;
use GoCRM\API\App\Repositories\FirmRepository;
use GoCRM\API\App\Repositories\UserRepository;
use GoCRM\API\Http\Client\HttpClient;


class GoCRM
{
    /**
     * @var FirmRepository
     */
    public $firms;

    /**
     * @var CityRepository
     */
    public $cities;

    /**
     * @var CenterRepository
     */
    public $centers;

    /**
     * @var UserRepository
     */
    public $users;

    /**
     * GoCRM constructor.
     * @param string $domain
     * @param string $appToken
     */
    public function __construct(string $domain, string $appToken)
    {
        $httpClient = new HttpClient($domain);
        $httpClient->setSecure(true);
        $httpClient->setAppToken($appToken);

        $this->firms = new FirmRepository($httpClient);
        $this->cities = new CityRepository($httpClient);
        $this->centers = new CenterRepository($httpClient);
        $this->users = new UserRepository($httpClient);
    }
}